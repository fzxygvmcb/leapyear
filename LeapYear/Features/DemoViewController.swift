//
//  DemoViewController.swift
//  LeapYear
//
//  Created by Anonymous on 12/12/2018.
//  Copyright © 2018 Kata. All rights reserved.
//

import UIKit

/// The minimum year selectable in the picker (set to `1800`)
fileprivate let minimumYear = 1800

/// The maximum year selectable in the picker (set to `3999`)
fileprivate let maximumYear = 3999

/// Responsible for displaying the demo screen
class DemoViewController: UIViewController {

    /// Label displaying the currently selected year leap status
    @IBOutlet private weak var labelResult: UILabel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Bundle.main.infoDictionary?["CFBundleName"] as? String ?? ""
        
        updateResult(with: minimumYear)
    }
}

// MARK: - Private
extension DemoViewController {
    
    /// Update the result label with the currently selected year leap status
    ///
    /// - Parameter year: the year selected in the picker
    private func updateResult(with year: Int) {
        labelResult.text = year.isLeapYear
            ? "\(year) is a leap year"
            : "\(year) is not a leap year"
    }
}

// MARK: - UIPickerViewDelegate
extension DemoViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(minimumYear + row)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateResult(with: minimumYear + row)
    }
}

// MARK: - UIPickerViewDataSource
extension DemoViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return maximumYear - minimumYear
    }
}
