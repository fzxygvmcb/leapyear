//
//  Int+LeapYear.swift
//  LeapYear
//
//  Created by Anonymous on 12/12/2018.
//  Copyright © 2018 Kata. All rights reserved.
//

import Foundation

extension Int {
    
    /// Get whether the current `Int` represents a leap year
    ///
    /// A year is considered a leap year if:
    /// - It is divisible by 400
    /// - It is divisible by 100 and by 400
    /// - It is divisible by 4 but not 100
    var isLeapYear: Bool {
        if self % 400 == 0 {
            // All year divisible by 400 are leap years
            return true
        }
        
        if self % 100 == 0 {
            // Years divisible by 100 but not 400 are NOT leap years
            return false
        }
        
        // At this point we know that year is neither divisible by 400 or by 100
        // Therefore `year` can only be a leap year if it is divisible by 4
        return self % 4 == 0
    }
}
